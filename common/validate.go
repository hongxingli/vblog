package common

import "github.com/go-playground/validator/v10"

func Validate(obj any) error {
	v := validator.New()
	return v.Struct(obj)
}
