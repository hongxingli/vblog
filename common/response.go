package common

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
)

//发送异常消息
func SendFaild(c *gin.Context, err error) {
	defer c.Abort()
	ae, ok := err.(*ApiException)
	if ok {
		c.JSON(ae.HttpCode, NewApiException(*ae.ErrorCode, ae.Message))
		return
	}
	//非自定义异常
	c.JSON(http.StatusInternalServerError, NewApiException(-1, err.Error()))

}

func NewApiException(code int, message string) *ApiException {
	return &ApiException{
		ErrorCode: &code,
		HttpCode:  code,
		Message:   message,
	}
}

//自定义异常
type ApiException struct {
	//int 默认为0 有歧义，所以用*int 默认为nil，状态表示未知
	ErrorCode *int `json:"error_code"`
	//http状态码
	HttpCode int `json:"http_code"`
	//异常的具体信息
	Message string `json:"message"`
}

func (e *ApiException) SetHttpCode(code int) *ApiException {
	e.HttpCode = code
	return e
}
func (e *ApiException) Error() string {
	return e.Message
}
func (e *ApiException) ToJson() string {
	d, _ := json.Marshal(e)
	return string(d)
}
