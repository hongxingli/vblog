package common

import "encoding/json"

func ToJson(obj any) string {
	dj, _ := json.MarshalIndent(obj, "", " ")
	return string(dj)
}
