package ioc

import (
	"fmt"
)

//定义map管理controller对象
var controller = map[string]IocObject{}

//对所有对象统一执行初始化
func InitController() error {
	for k, v := range controller {
		err := v.Init()
		if err != nil {
			return fmt.Errorf("init %s error,%s", k, err)
		}
	}
	return nil
}

//托管业务实现的类
func RegistryContrlller(obj IocObject) {
	controller[obj.Name()] = obj
}

//获取托管
func GetController(name string) any {
	v, ok := controller[name]
	if !ok {
		panic("controller not found")
	}
	return v
}

//打印托管的实例
func ShowControllers() (names []string) {
	for k, _ := range controller {
		names = append(names, k)
	}
	return names
}
