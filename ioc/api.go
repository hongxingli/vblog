package ioc

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"path"
)

// 为啥要单独管理API Handler, API Handler 有个Registry() 注册路由的特色方法
type IocApi interface {
	IocObject
	// 提供 业务Handler给Root路由的能力
	Registry(r gin.IRouter)
}

// 管理Controller对象
// 采用一个map来存储对象, 存储在内存中, 程序启动 解决多个impl对象的依赖问题
var api = map[string]IocApi{}

// 对所有的对象统一执行 初始化
// 把所有 api handler 注册root 路由
func InitHttpApi(pathPrefix string, r gin.IRouter) error {
	for k, v := range api {
		err := v.Init()
		if err != nil {
			return fmt.Errorf("init %s error, %s", k, err)
		}

		//为每一个模块生成一个子路由
		// /vblog/api/v1 + blogs
		v.Registry(r.Group(path.Join(pathPrefix, k)))

	}
	return nil
}

// 托管业务实现的类
func RegistryHttpApi(obj IocApi) {
	api[obj.Name()] = obj
}

//打印托管的实例
func ShowApis() (names []string) {
	for k, _ := range api {
		names = append(names, k)
	}
	return names
}
