package ioc

type IocObject interface {
	// 用于初始化对象
	Init() error
	// 对象的名称
	Name() string
}
