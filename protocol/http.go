package protocol

import (
	"context"
	"gitee.com/hongxingli/vblog/conf"
	"gitee.com/hongxingli/vblog/logger"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

//http具体使用哪个路由，就把谁当参数传递进来
func NewHttp(r *gin.Engine) *Http {
	gin.Default()
	return &Http{
		server: &http.Server{
			//Addr: "0.0.0.0:8080" //不建议写死
			//也不建议这样使用，1、其他地方使用还得再写一次，return 写过程。把处理的逻辑封装到对象上
			//Addr: fmt.Sprintf("%s:%d",conf.C().Http.Host,conf.C().Http.Port),
			Addr: conf.C().Http.Address(),
			//配置http server使用的路由，如果是其他web框架就封装其他的进来
			Handler:      r,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 5 * time.Second,
		},
	}
}

//自己封装一个http server
type Http struct {
	server *http.Server
}

func (h *Http) Start() error {
	logger.L().Debug().Msgf("http server :%s", conf.C().Http.Address())
	//http 80 监听
	return h.server.ListenAndServe()
}
func (h *Http) Stop(ctx context.Context) {
	//优雅关闭
	//自己处理根据ctx信号处理怎么关闭
	h.server.Shutdown(ctx)
}
