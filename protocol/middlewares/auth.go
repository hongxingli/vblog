package middlewares

import (
	"fmt"
	"gitee.com/hongxingli/vblog/apps/user"
	"gitee.com/hongxingli/vblog/common"
	"gitee.com/hongxingli/vblog/ioc"
	"github.com/gin-gonic/gin"
	"net/http"
)

//r.use() 根据gin的use函数可以看到middleware的实现方式

// Use attaches a global middleware to the router. i.e. the middleware attached through Use() will be
// included in the handlers chain for every single request. Even 404, 405, static files...
// For example, this is the right place for a logger or error management middleware.
//func (engine *Engine) Use(middleware ...HandlerFunc) IRoutes {
//	engine.RouterGroup.Use(middleware...)
//	engine.rebuild404Handlers()
//	engine.rebuild405Handlers()
//	return engine
//}

func Auth(c *gin.Context) {
	//在这里写具体的处理认证逻辑
	//1、获取token
	//2、获取user validate token,校验token合法性
	//3、把user对象放到ctx中，后面createblog需要从上下文获取用户的身份信息
	fmt.Println("auth.....")

	tk, err := c.Cookie(user.AUTH_COOKIE_NAME)
	fmt.Println("this is token:", tk)
	if err != nil {
		common.SendFaild(c, err)
	}
	if tk == "" {
		common.SendFaild(c, common.NewApiException(http.StatusUnauthorized, "need login"))
	}
	usvc := ioc.GetController(user.AppName).(user.Service)
	tkobj, err := usvc.ValiateToken(c.Request.Context(), user.NewValidateTokenRequest(tk))
	if err != nil {
		common.SendFaild(c, err)
		return
	}
	c.Set(user.REQUEST_CTX_TOKEN_KEY, tkobj)
}
