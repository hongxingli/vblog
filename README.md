## Vblog项目学习golang使用
目录结构：
+ apps  TDD模式
  + -blog blog模块
   + -api 业务接口实现
   + -impl 业务逻辑的实现
  + -comment  评论模块
+ conf 项目配置管理
+ protocol 协议服务器  处理对应的协议
+ etc 程序加载配置文件目录
+ cmd cli
  + -start 启动项目cli实现
+ common  通用工具包
+ logger 日志处理
+ ioc 实现一个对象托管容器（管理对象的一个中介）
+ ui 前端 vue+arcodesign

+ mvc  mvc模式
-----
## 项目开发 梳理vblog
项目骨架定义DDD方式（流程是从上到下）
+ 业务处理模块：每个业务一个模块
    + 流程：  
      先定义业务，即内部用的接口Service interface，声明的好数据结构，定义数据库的表结构。  
      定义好了接口，采用impl实例类去实现上述定义的业务接口  
      然后对外进行暴漏api，采用gin，handler，从用户的http请求读取参数，然后调用业务层svc的实力类去处理，api不包含业务逻辑，仅仅在处理网络请求。
+ 项目配置管理 pkg conf 项目的运行参数，通过配置传递给程序，管理着整个项目的配置
  使用结构体Config定义项目配置信息,使用load方法去读取配置
+ 项目的文档 docs
+ 项目的接口 protocol 协议服务器，监听对应的端口（http/grpc）
+ 项目CLI cmd项目的cli工具库 cobra
+ 项目入口文件 main.go 项目所有的包类在main.go进行程序组装



+ 程序的配置文件  
  文件格式的配置 config.toml  
  环境变量的配置 config.env  
  程序样例配置 config.example.toml  
  单元测试配置 unit_test.env (vscode使用 goland还没找怎么做，待实现)

------ 
## v2版本 ioc重构
v1版本多个应用就要注册多次（比如上个commit没解决的），v2新增ioc，从ioc中获取依赖对象  
 1、先写业务实现  
 2、再写接口实现  
 3、注册到ioc
 4、程序启动的时候, 注册所有的实例: apps/registry.go


