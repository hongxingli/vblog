CREATE TABLE `users` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `created_at` int(10) NOT NULL,
                         `updated_at` int(10) NOT NULL,
                         `username` varchar(255) NOT NULL,
                         `password` varchar(255) NOT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `idx_user` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1