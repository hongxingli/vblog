CREATE TABLE `blogs` (
                         `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '文章的Id',
                         `tags` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签',
                         `create_at` int NOT NULL COMMENT '创建时间',
                         `publish_at` int NOT NULL COMMENT '发布时间',
                         `update_at` int NOT NULL COMMENT '更新时间',
                         `title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章标题',
                         `author` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '作者',
                         `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章内容',
                         `status` tinyint NOT NULL COMMENT '文章状态',
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `idx_title` (`title`) COMMENT 'titile添加唯一键约束'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
