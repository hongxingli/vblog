package main

import (
	"gitee.com/hongxingli/vblog/cmd"
	"github.com/spf13/cobra"
)

func main() {
	//简单的方式：启动一个gin web框架，但当定制http参数的时候 无法定制 比如超时时间
	//如何实现？自己去封装一个http server 详细在protocol包
	//engine := gin.Default()
	//engine.Run("127.0.0.1:8000")

	err := cmd.Execute()
	cobra.CheckErr(err)
}
