package start

import (
	_ "gitee.com/hongxingli/vblog/apps"
	"gitee.com/hongxingli/vblog/conf"
	"gitee.com/hongxingli/vblog/ioc"
	"gitee.com/hongxingli/vblog/protocol"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
)

var (
	configType string
	configFile string
)

var Cmd = &cobra.Command{
	Use:   "start",
	Short: "start vblog后端",
	Long:  "",
	Run: func(cmd *cobra.Command, args []string) {
		//命令的具体实现，程序的组装
		//1、加载全局配置
		switch configType {
		case "env":
			_, err := conf.LoadConfigFromEnv()
			cobra.CheckErr(err)
		default:
			_, err := conf.LoadConfigFromToml(configFile)
			cobra.CheckErr(err)
		}

		//v1版 加载业务逻辑实现
		r := gin.Default()
		////blog业务模块
		//blogService := &impl.Impl{}
		//err := blogService.Init()
		//cobra.CheckErr(err)
		//apiHandler := api.NewHandler(blogService)
		//apiHandler.Registry(r)

		//fmt.Println("我要开始注册comment了")
		////comment业务模块
		//commentService := &commentimpl.Impl{}
		//err = commentService.Init()
		//cobra.CheckErr(err)
		//commentapiHandler := commentapi.NewHandler(commentService)
		//commentapiHandler.Registry(r)
		//fmt.Println("comment 注册完了")

		//v2版 采用ioc实现
		//注册到ioc里的对象进行初始化
		//logger.L().Debug().Msgf("controller: %s", ioc.ShowControllers())
		//logger.L().Debug().Msgf("api: %s", ioc.ShowApis())

		err := ioc.InitController()
		cobra.CheckErr(err)
		err = ioc.InitHttpApi("/vblog/api/v1", r)
		cobra.CheckErr(err)

		//启动协议服务器
		//fmt.Println(conf.C().Http.Address())
		httpServer := protocol.NewHttp(r)
		err = httpServer.Start()
		cobra.CheckErr(err)

		//【TODO】等老喻帮忙解释中 我自己不明白
		//加载comment模块[下面的有问题，comment get不出来路由。如果把comment放到上面，那么blog就不会get出路由了]
		//自己是不理解http导致的上述问题 上面已经start ListenAndServe 了下面这些自然就不在执行了
		//fmt.Println("我要开始注册comment了")
		//r1 := gin.Default()
		////comment业务模块
		//commentService := &commentimpl.Impl{}
		//err = commentService.Init()
		//cobra.CheckErr(err)
		//commentapiHandler := commentapi.NewHandler(commentService)
		//commentapiHandler.Registry(r1)
		//fmt.Println("comment 注册完了")
		//启动协议服务器
		//fmt.Println(conf.C().Http.Address())
		//httpServer = protocol.NewHttp(r1)
		//err = httpServer.Start()
		//cobra.CheckErr(err)
	},
}

func init() {
	Cmd.Flags().StringVarP(&configType, "config-type", "t", "file", "程序加载配置的方式")
	//Cmd.Flags().StringVarP(&configFile, "config-file", "f", "file", "配置文件的路径")
	//file 给个默认值etc/config.toml
	Cmd.Flags().StringVarP(&configFile, "config-file", "f", "etc/config.toml", "配置文件的路径")

}
