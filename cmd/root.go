package cmd

import (
	"gitee.com/hongxingli/vblog/cmd/start"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "vblog-api",
	Short: "vblog后端",
	Long:  "",
	Run: func(cmd *cobra.Command, args []string) {
		//命令的具体实现
		cmd.Help()
	},
}

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	//子commond完成注册
	rootCmd.AddCommand(start.Cmd)
}
