package user

import (
	"context"
)

type Service interface {
	CreateUser(ctx context.Context, request *CreateUserRequest) (*User, error)

	DescribeUser(ctx context.Context, request *DescribeUserRequest) (*User, error)

	Login(ctx context.Context, request *LoginRequest) (*Token, error)

	ValiateToken(ctx context.Context, request *ValiateTokenRequest) (*Token, error)
}

//防止读代码产生歧义
type LoginRequest struct {
	*CreateUserRequest
}

func NewLoginRequest() *LoginRequest {
	return &LoginRequest{
		CreateUserRequest: NewCreateUserRequest(),
	}
}

func NewValidateTokenRequest(ak string) *ValiateTokenRequest {
	return &ValiateTokenRequest{
		AccessToken: ak,
	}
}

type ValiateTokenRequest struct {
	AccessToken string
}

type DESCRIBE_BY int

const (
	DESCRIBE_BY_NAME DESCRIBE_BY = iota
	DESCRIBE_BY_ID
)

type DescribeUserRequest struct {
	DescribeBy    DESCRIBE_BY
	DescribeValue string
}

func NewDescribeUserRequest(name string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy:    DESCRIBE_BY_NAME,
		DescribeValue: name,
	}
}
