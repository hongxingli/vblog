package impl_test

import (
	"context"
	"gitee.com/hongxingli/vblog/apps/user"
	"gitee.com/hongxingli/vblog/ioc"
	"gitee.com/hongxingli/vblog/test"
)

var (
	controller user.Service
	ctx        = context.Background()
)

func init() {
	test.DevelopmentSetUp()
	controller = ioc.GetController(user.AppName).(user.Service)
}
