package impl

import (
	"context"
	"fmt"
	"gitee.com/hongxingli/vblog/apps/user"
	"gitee.com/hongxingli/vblog/common"
	"net/http"
)

func (i *impl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (*user.User, error) {
	if err := in.Validate(); err != nil {
		return nil, common.NewApiException(http.StatusBadRequest, err.Error())
	}
	ins := user.NewUser(in)
	err := ins.BuildHashPassword()
	err = i.db.WithContext(ctx).Save(ins).Error
	if err != nil {
		return nil, common.NewApiException(http.StatusInternalServerError, err.Error())
	}

	return ins, nil
}

func (i *impl) DescribeUser(ctx context.Context, in *user.DescribeUserRequest) (*user.User, error) {
	query := i.db.WithContext(ctx).Model(&user.User{})
	switch in.DescribeBy {
	case user.DESCRIBE_BY_ID:
		query.Where("id = ?", in.DescribeValue)
	case user.DESCRIBE_BY_NAME:
		query.Where("username = ?", in.DescribeValue)
	default:
		return nil, fmt.Errorf("unknown describe by")
	}
	ins := user.NewDefaultUser()
	err := query.First(ins).Error
	if err != nil {
		return nil, err
	}

	return ins, nil
}
func (i *impl) Login(ctx context.Context, in *user.LoginRequest) (*user.Token, error) {
	//获取用户
	u, err := i.DescribeUser(ctx, user.NewDescribeUserRequest(in.Username))
	if err != nil {
		return nil, err
	}
	//验证密码
	err = u.CheckPasword(in.Password)
	if err != nil {
		return nil, common.NewApiException(http.StatusUnauthorized, err.Error())
	}
	//颁发token
	tk := user.NewToken(in.Username)
	err = i.db.Save(tk).Error
	if err != nil {
		return nil, err
	}
	return tk, nil
}

func (i *impl) ValiateToken(ctx context.Context, in *user.ValiateTokenRequest) (*user.Token, error) {
	tk := user.NewToken("")
	err := i.db.
		WithContext(ctx).
		Model(&user.Token{}).
		Where("access_token = ?", in.AccessToken).
		First(tk).
		Error
	fmt.Println("valiatetoke is :", tk)
	fmt.Println("inacfesstoken:", in.AccessToken)
	fmt.Println(err)
	if err != nil {
		return nil, err
	}
	return tk, nil
}
