package impl

import (
	"gitee.com/hongxingli/vblog/apps/user"
	"gitee.com/hongxingli/vblog/conf"
	"gitee.com/hongxingli/vblog/ioc"
	"gorm.io/gorm"
)

type impl struct {
	db *gorm.DB
}

func (i *impl) Init() error {
	db := conf.C().MySQL.ORM()
	i.db = db.Debug()
	return nil
}

func (i *impl) Name() string {
	return user.AppName
}
func init() {
	ioc.RegistryContrlller(&impl{})
}
