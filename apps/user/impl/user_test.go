package impl_test

import (
	"gitee.com/hongxingli/vblog/apps/user"
	"testing"
)

func TestCreateUser(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.Username = "admin3"
	req.Password = "123456"
	ins, err := controller.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestDescribeUser(t *testing.T) {
	req := user.NewDescribeUserRequest("admin")
	ins, err := controller.DescribeUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
	t.Log(ins.CheckPasword("123456"))
}

func TestLogin(t *testing.T) {
	req := user.NewLoginRequest()
	req.Username = "admin"
	req.Password = "123456"
	ins, err := controller.Login(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestValiateToken(t *testing.T) {
	req := user.NewValidateTokenRequest("ci2qpt6ed2fjhh258160")
	ins, err := controller.ValiateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
