package user

import (
	"gitee.com/hongxingli/vblog/common"
	"github.com/rs/xid"
	"golang.org/x/crypto/bcrypt"
)

func NewToken(username string) *Token {
	return &Token{
		Meta:     common.NewMeta(),
		Username: username,
		//生成随机字符串用作token
		AccessToken: xid.New().String(),
	}
}

type Token struct {
	*common.Meta
	AccessToken string `json:"token" validate:"required"`
	Username    string `json:"username" validate:"required"`
}

func (t *Token) String() string {
	return common.ToJson(t)
}

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{}
}

type CreateUserRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func NewDefaultUser() *User {
	return NewUser(NewCreateUserRequest())
}

func NewUser(req *CreateUserRequest) *User {
	return &User{
		Meta:              common.NewMeta(),
		CreateUserRequest: req,
	}
}

type User struct {
	*CreateUserRequest
	*common.Meta
}

func (u *User) String() string {
	return common.ToJson(u)
}

//把密码hash
func (u *User) BuildHashPassword() error {
	//v1 low版
	//h := sha1.New()
	//_, err := h.Write([]byte(u.Password))
	//if err != nil {
	//	return err
	//}
	//hd := h.Sum(nil)
	//u.Password = base64.StdEncoding.EncodeToString(hd)
	//return nil

	//v2
	hp, err := bcrypt.GenerateFromPassword([]byte(u.Password), 10)
	if err != nil {
		return err
	}
	u.Password = string(hp)
	return nil
}

//校验密码
func (u *User) CheckPasword(planPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(planPassword))
}

func (t *Token) TableName() string {
	return "tokens"
}

func (U *User) TableName() string {
	return "users"
}

func (r *CreateUserRequest) Validate() error {
	return common.Validate(r)
}
