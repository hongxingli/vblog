package api

import (
	"gitee.com/hongxingli/vblog/apps/user"
	"gitee.com/hongxingli/vblog/common"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *Handler) Login(c *gin.Context) {
	in := user.NewLoginRequest()
	err := c.BindJSON(in)
	if err != nil {
		common.SendFaild(c, err)
		return
	}
	ins, err := h.svc.Login(c.Request.Context(), in)
	if err != nil {
		common.SendFaild(c, err)
		return
	}
	c.SetCookie(user.AUTH_COOKIE_NAME, ins.AccessToken, 36000, "/", "127.0.0.1", false, false)
	c.JSON(http.StatusOK, ins)
}
