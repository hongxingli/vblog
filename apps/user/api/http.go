package api

import (
	"gitee.com/hongxingli/vblog/apps/user"
	"gitee.com/hongxingli/vblog/ioc"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	svc user.Service
}

func (h *Handler) Init() error {
	h.svc = ioc.GetController(user.AppName).(user.Service)
	return nil
}
func (h *Handler) Name() string {
	return user.AppName
}
func (h *Handler) Registry(r gin.IRouter) {
	//ioc 内已经加了子路由这里就要改改
	///vblog/api/v1/users + login
	r.POST("/login", h.Login)
}
func init() {
	ioc.RegistryHttpApi(&Handler{})
}
