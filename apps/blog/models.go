package blog

import (
	"encoding/json"
	"gitee.com/hongxingli/vblog/common"
	"time"
)

func NewBlogSet() *BlogSet {
	return &BlogSet{
		Items: []*Blog{},
	}
}

func (s *BlogSet) String() string {
	dj, _ := json.MarshalIndent(s, "", "  ")
	return string(dj)
}

type BlogSet struct {
	Total int64   `json:"total"`
	Items []*Blog `json:"items"`
}

func (i *Blog) TableName() string {
	return "blogs"
}

func NewBlog(req *CreateRequestBlog) *Blog {
	return &Blog{
		Meta:              NewMeta(),
		CreateRequestBlog: req,
	}
}

type Blog struct {
	*Meta
	*CreateRequestBlog
}

func NewMeta() *Meta {
	return &Meta{
		CreateAt: time.Now().Unix(),
	}
}

type Meta struct {
	Id        int   `json:"id"`
	CreateAt  int64 `json:"create_at"`
	UpdateAt  int64 `json:"update_at"`
	PublishAt int64 `json:"publish_at"`
}

func NewCreateRequestBlog() *CreateRequestBlog {
	return &CreateRequestBlog{
		Tags:   map[string]string{},
		Status: STATUS_DRAFT,
	}
}

//gorm tag不定义时，默认使用json的tag
type CreateRequestBlog struct {
	Title   string            `json:"title" gorm:"column:title" validate:"required"`
	Author  string            `json:"author" gorm:"column:author"`
	Content string            `json:"content"  validate:"required"`
	Tags    map[string]string `json:"tags" gorm:"serializer:json"`
	Status  STATUS            `json:"status"`
}

func (req *CreateRequestBlog) Validate() error {
	return common.Validate(req)
}
