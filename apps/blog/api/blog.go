package api

import (
	"fmt"
	"gitee.com/hongxingli/vblog/apps/blog"
	"gitee.com/hongxingli/vblog/apps/user"
	"gitee.com/hongxingli/vblog/common"
	"gitee.com/hongxingli/vblog/logger"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func (h *Handler) CreateBlog(c *gin.Context) {
	//c.Request.Body
	//原始方式
	//io.ReadAll(c.Request.Body)
	//gin body ---> json.umarshal

	//获取用户信息
	v, ok := c.Get(user.REQUEST_CTX_TOKEN_KEY)
	fmt.Println("get token is:", v)
	if !ok {
		common.SendFaild(c, common.NewApiException(http.StatusUnauthorized, "need auth"))
		return
	}
	u := v.(*user.Token)
	logger.L().Debug().Msgf("login user: %s", u)
	in := blog.NewCreateRequestBlog()
	fmt.Println("in:", in)
	err := c.BindJSON(in)
	fmt.Println("err:", err)
	if err != nil {
		//如何统一处理异常，异常应该返回的数据格式
		//common.SendFaild(c, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		common.SendFaild(c, err)
		return
	}

	ins, err := h.svc.CreateBlog(c.Request.Context(), in)
	if err != nil {
		//common.SendFaild(c, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		common.SendFaild(c, err)
		c.AbortWithError(http.StatusOK, err)
		return
	}
	c.JSON(http.StatusOK, ins)

}

//查询
func (h *Handler) QueryBlog(c *gin.Context) {
	//初始化一个请求对象
	in := blog.NewQueryBlogRequest()
	in.PageSize, _ = strconv.Atoi(c.DefaultQuery("page_size", "20"))
	in.PageNumber, _ = strconv.Atoi(c.DefaultQuery("page_number", "1"))
	in.Author = c.Query("author")
	in.Keywords = c.Query("keywords")

	ins, err := h.svc.QueryBlog(c.Request.Context(), in)
	if err != nil {
		//common.SendFaild(c, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		common.SendFaild(c, err)
		c.AbortWithError(http.StatusOK, err)
		return
	}
	c.JSON(http.StatusOK, ins)

}
