package api

import (
	"fmt"
	"gitee.com/hongxingli/vblog/apps/blog"
	"gitee.com/hongxingli/vblog/ioc"
	"gitee.com/hongxingli/vblog/protocol/middlewares"
	"github.com/gin-gonic/gin"
)

func NewHandler() *Handler {
	return &Handler{}
}

type Handler struct {
	svc blog.Service
}

//从ioc中获取依赖的对象
func (h *Handler) Init() error {
	//获取的是ioc any对象，强行类型断言成正在提供服务的业务对象
	h.svc = ioc.GetController(blog.AppName).(blog.Service)
	return nil
}
func (h *Handler) Name() string {
	return blog.AppName
}

func (h *Handler) Registry(r gin.IRouter) {
	fmt.Println("blog registry查询博客列表页开始注册 -----")
	//sr := r.Group("/vblog/api/v1/blogs")
	r.GET("", h.QueryBlog)

	//curl --location '127.0.0.1:8090/vblog/api/v1/blogs' \
	//--header 'Content-Type: text/plain' \
	//--header 'Cookie: access-token=ci6mcrmed2ft4e72q15g; token=ci6muheed2fot61ia9d0' \
	//--data '{
	//    "title":"myblog middleware",
	//    "content":"this is use middleware and cookie"
	//}'
	r.Use(middlewares.Auth)
	fmt.Println("blog registry -----")
	r.POST("", h.CreateBlog)
	fmt.Println("blog registry end -----")

}

func init() {
	ioc.RegistryHttpApi(&Handler{})
}
