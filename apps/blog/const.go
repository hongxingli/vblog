package blog

import "fmt"

type STATUS int

//自定义json输出的内容，实现MarshalJson方法
func (s STATUS) MarshalJSON() ([]byte, error) {
	switch s {
	case STATUS_DRAFT:
		return []byte("\"草稿\""), nil
	case STATUS_PUBLISHED:
		return []byte("\"已发布\""), nil
	}
	return []byte(fmt.Sprintf("%d", s)), nil
}

const (
	STATUS_DRAFT STATUS = iota
	STATUS_PUBLISHED
)

const (
	//业务单元的名字
	//定义ioc获取的name名字
	AppName = "blogs"
)
