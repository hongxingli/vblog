package blog

import "context"

// CRUD
type Service interface {
	CreateBlog(context.Context, *CreateRequestBlog) (*Blog, error)

	QueryBlog(context.Context, *QueryBlogRequest) (*BlogSet, error)

	DescribeBlog(context.Context, *DescribeBlogRequest) (*Blog, error)

	UpdateBlog(context.Context, *UpdateBlogRequest) (*Blog, error)

	DeleteBlog(context.Context, *DeleteBlogRequest) (*Blog, error)
}

func NewQueryBlogRequest() *QueryBlogRequest {
	return &QueryBlogRequest{
		PageSize:   20,
		PageNumber: 1,
	}
}

type QueryBlogRequest struct {
	PageSize   int
	PageNumber int
	Keywords   string
	Author     string
}

func (r *QueryBlogRequest) Offset() int {
	return (r.PageNumber - 1) * r.PageSize
}

type DescribeBlogRequest struct {
}
type UpdateBlogRequest struct {
	*CreateRequestBlog
}
type DeleteBlogRequest struct {
}
