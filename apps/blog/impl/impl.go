package impl

import (
	"gitee.com/hongxingli/vblog/apps/blog"
	"gitee.com/hongxingli/vblog/conf"
	"gitee.com/hongxingli/vblog/ioc"
	"gorm.io/gorm"
)

var _ blog.Service = &impl{}

// 先定义一个对象，由这个对象来实现业务功能
type impl struct {
	db *gorm.DB
}

//业务实例如何访问配置，通常为这个实例类，提供初始化方法
func (i *impl) Init() error {
	db := conf.C().MySQL.ORM()
	i.db = db.Debug()
	return nil
}
func (i *impl) Name() string {
	return blog.AppName
}

//通过import的方式，自动调用init 把对象注入到ioc。需要实现Init 和 Name的方法
func init() {
	//已经被约束过的对象不会在存在其他的方法 var impl blog.Service = &Impl{}，这里只能传&Impl{}
	ioc.RegistryContrlller(&impl{})
}
