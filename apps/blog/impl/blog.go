package impl

import (
	"context"
	"gitee.com/hongxingli/vblog/apps/blog"
	"gitee.com/hongxingli/vblog/common"
	"net/http"
)

func (i *impl) CreateBlog(ctx context.Context, in *blog.CreateRequestBlog) (*blog.Blog, error) {
	//检查用户参数
	if err := in.Validate(); err != nil {
		return nil, common.NewApiException(http.StatusBadRequest, err.Error())
	}

	ins := blog.NewBlog(in)

	err := i.db.WithContext(ctx).Save(ins).Error
	if err != nil {
		return nil, common.NewApiException(http.StatusInternalServerError, err.Error())
	}
	return ins, err
}

func (i *impl) QueryBlog(ctx context.Context, in *blog.QueryBlogRequest) (*blog.BlogSet, error) {
	query := i.db.WithContext(ctx).Model(&blog.Blog{})

	set := blog.NewBlogSet()

	if in.Keywords != "" {
		//like 模糊搜索
		query = query.Where("content like ?", "%"+in.Keywords+"%")
	}
	if in.Author != "" {
		query = query.Where("author = ?", in.Author)
	}
	err := query.Count(&set.Total).Offset(int(in.Offset())).Limit(in.PageSize).Find(&set.Items).Error
	if err != nil {
		return nil, err
	}
	return set, nil
}

func (i *impl) DescribeBlog(context.Context, *blog.DescribeBlogRequest) (*blog.Blog, error) {
	return nil, nil
}

func (i *impl) UpdateBlog(context.Context, *blog.UpdateBlogRequest) (*blog.Blog, error) {
	return nil, nil
}

func (i *impl) DeleteBlog(context.Context, *blog.DeleteBlogRequest) (*blog.Blog, error) {
	return nil, nil
}
