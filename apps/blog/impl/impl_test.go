package impl_test

import (
	"context"
	"gitee.com/hongxingli/vblog/apps/blog"
	"gitee.com/hongxingli/vblog/ioc"
	"gitee.com/hongxingli/vblog/test"
)

//初始化需要被测试的对象
var (
	controller blog.Service
)

func init() {
	test.DevelopmentSetUp()
	controller = ioc.GetController(blog.AppName).(blog.Service)
}

//func init() {
//	_, err := conf.LoadConfigFromEnv()
//	if err != nil {
//		panic(err)
//	}
//	obj := &impl.Impl{}
//	if err := obj.Init(); err != nil {
//		panic(err)
//	}
//	controller = obj
//	//controller = &Mock{}
//}
//
type Mock struct {
}

func (m *Mock) CreateBlog(ctx context.Context, in *blog.Blog) error {
	return nil
}
