package impl_test

import (
	"context"
	"gitee.com/hongxingli/vblog/apps/blog"
	"gitee.com/hongxingli/vblog/common"
	"testing"
)

var (
	ctx = context.Background()
)

func TestCreateBlog(t *testing.T) {
	in := blog.NewCreateRequestBlog()
	in.Title = "test"
	in.Author = "lihongxing"
	in.Content = "hello world welcome"
	ins, err := controller.CreateBlog(ctx, in)
	if err != nil {
		//断言 获取自定义异常
		ae, ok := err.(*common.ApiException)
		if ok {
			t.Log(ae.ToJson())
		}
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryBlogSet(t *testing.T) {
	req := blog.NewQueryBlogRequest()
	//测试模糊搜索
	req.Keywords = "go"
	set, err := controller.QueryBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
