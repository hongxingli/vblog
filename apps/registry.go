package apps

import (
	_ "gitee.com/hongxingli/vblog/apps/blog/api"
	_ "gitee.com/hongxingli/vblog/apps/blog/impl"
	_ "gitee.com/hongxingli/vblog/apps/comment/api"
	_ "gitee.com/hongxingli/vblog/apps/comment/impl"
	_ "gitee.com/hongxingli/vblog/apps/user/api"
	_ "gitee.com/hongxingli/vblog/apps/user/impl"
)
