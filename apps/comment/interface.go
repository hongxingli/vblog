package comment

import "context"

//2、定义评论的接口
type Service interface {
	CreateComment(ctx context.Context, request *CreateCommentRequest) (*Comment, error)
}
