package api

import (
	"gitee.com/hongxingli/vblog/apps/comment"
	"gitee.com/hongxingli/vblog/common"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

//6、对外暴漏评论的api,使用gin的handler
//api仅仅是在处理网络请求 不包含业务逻辑 该函数不能有返回值 不然在gin的r.GET("/vblog/api/v1/comments", h.CreateComment) 不符合GET的传参
func (h *Handler) CreateComment(c *gin.Context) {

	//调用业务逻辑处理业务逻辑
	in := comment.NewCommentCreateRequest()
	err := c.BindJSON(in)
	if err != nil {
		common.SendFaild(c, err)
		return
	}
	ins, err := h.svc.CreateComment(c.Request.Context(), in)

	if err != nil {
		log.Fatal(err)
	}
	c.JSON(http.StatusOK, ins)
}
