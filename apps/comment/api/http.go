package api

import (
	"gitee.com/hongxingli/vblog/apps/comment"
	"gitee.com/hongxingli/vblog/ioc"
	"github.com/gin-gonic/gin"
)

func NewHandler(svc comment.Service) *Handler {
	return &Handler{
		svc: svc,
	}
}

//5、定义处理http报文的类，让这个handler去实现具体的aAPI接口
type Handler struct {
	svc comment.Service
}

func (h *Handler) Registry(r gin.IRouter) {
	//ioc 加了子路由 这里要改改
	///vblog/api/v1/ + comments
	r.GET("", h.CreateComment)

}

func (h *Handler) Init() error {
	h.svc = ioc.GetController(comment.AppName).(comment.Service)
	return nil
}
func (h *Handler) Name() string {
	return comment.AppName
}

func init() {
	ioc.RegistryHttpApi(&Handler{})
}
