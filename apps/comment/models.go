package comment

import "gitee.com/hongxingli/vblog/common"

//1、定义评论表结构
type Comment struct {
	//1、当这里没有id字段的时候 执行的update
	//UPDATE `t_comments` SET `u_id`=1,`b_id`=2,`content`='这是一个测试'
	//2、save 方法解释 If value doesn't contain a matching primary key, value is inserted
	//如果我把主键改为u_id，直接传，他也会INSERT INTO `t_comments` (`u_id`,`b_id`,`content`) VALUES (1,2,'这是一个测试')
	//和save定义的感觉不一样呢，不懂
	//3、如果注释掉id字段，删掉数据库id，数据库把b_id设置成主键，这样才是update，这样感觉和save的解释匹配
	//4、不管有没有主键，数据库也删掉id字段，注释掉底下的Id字段，是update
	//5、不管有没有主键，删掉数据库id字段，不注释底下Id字段，是insert
	//6、自己总结，综上所述，只要这个models里定义id字段，他就会insert，
	Id int `json:"id"`
	*CreateCommentRequest
}

func NewComment(req *CreateCommentRequest) *Comment {
	return &Comment{
		CreateCommentRequest: req,
	}
}

func NewCommentCreateRequest() *CreateCommentRequest {
	return &CreateCommentRequest{}
}

type CreateCommentRequest struct {
	UserId  int    `json:"u_id"  gorm:"column:u_id"`
	BlogId  int    `json:"b_id"  gorm:"column:b_id"`
	Content string `json:"content"`
}

func NewCreateCommentRequest() *CreateCommentRequest {
	return &CreateCommentRequest{}
}

func (c *Comment) TableName() string {
	return "t_comments"
}

func (req *CreateCommentRequest) Validate() error {
	return common.Validate(req)
}
