package impl

import (
	"gitee.com/hongxingli/vblog/apps/comment"
	"gitee.com/hongxingli/vblog/conf"
	"gitee.com/hongxingli/vblog/ioc"
	"gorm.io/gorm"
)

var _ comment.Service = &Impl{}

//3、定义一个类 让这个对象去实现评论
type Impl struct {
	db *gorm.DB
}

//业务实例如何访问配置，通常为这个实例类，提供初始化方法
func (i *Impl) Init() error {
	db := conf.C().MySQL.ORM()
	i.db = db.Debug()
	return nil
}

func (i *Impl) Name() string {
	return comment.AppName
}
func init() {
	ioc.RegistryContrlller(&Impl{})
}
