package impl_test

import (
	"context"
	"gitee.com/hongxingli/vblog/apps/comment"
	"gitee.com/hongxingli/vblog/ioc"
	"gitee.com/hongxingli/vblog/test"
)

var (
	controller comment.Service
	ctx        = context.Background()
)

func init() {
	test.DevelopmentSetUp()
	controller = ioc.GetController(comment.AppName).(comment.Service)
}
