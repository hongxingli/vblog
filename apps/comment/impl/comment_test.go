package impl_test

import (
	"gitee.com/hongxingli/vblog/apps/comment"
	"testing"
)

func TestComment(t *testing.T) {
	req := comment.NewCommentCreateRequest()
	req.UserId = 1
	req.BlogId = 2
	req.Content = "这是一个测试"
	ins, err := controller.CreateComment(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
