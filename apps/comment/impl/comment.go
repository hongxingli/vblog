package impl

import (
	"context"
	"gitee.com/hongxingli/vblog/apps/comment"
	"gitee.com/hongxingli/vblog/common"
	"net/http"
)

//4、实现CreateComment方法
func (i *Impl) CreateComment(ctx context.Context, in *comment.CreateCommentRequest) (*comment.Comment, error) {
	if err := in.Validate(); err != nil {
		return nil, common.NewApiException(http.StatusBadRequest, err.Error())
	}
	ins := comment.NewComment(in)
	err := i.db.WithContext(ctx).
		Save(ins).
		Error
	if err != nil {
		return nil, common.NewApiException(http.StatusInternalServerError, err.Error())
	}
	return ins, nil
}
