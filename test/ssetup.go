package test

import (
	_ "gitee.com/hongxingli/vblog/apps"
	"gitee.com/hongxingli/vblog/conf"
	"gitee.com/hongxingli/vblog/ioc"
)

//开发环境的单元测试初始化ioc
func DevelopmentSetUp() {
	_, err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}
	err = ioc.InitController()
	if err != nil {
		panic(err)
	}
}
