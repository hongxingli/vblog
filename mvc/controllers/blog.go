package controllers

import (
	"context"
	"gitee.com/hongxingli/vblog/mvc/dao"
	"gitee.com/hongxingli/vblog/mvc/models"
)

//业务逻辑处理
func CreateBlog(ctx context.Context, ins *models.Blog) error {

	return dao.SaveBlog(ins)
}
