package api

import (
	"gitee.com/hongxingli/vblog/mvc/controllers"
	"gitee.com/hongxingli/vblog/mvc/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

func HandleCreateBlog(c gin.Context) {
	c.GetHeader("CROS_TOKEN")
	c.Param("id")
	c.Query("id")
	ins := &models.Blog{}
	c.BindJSON(ins)
	err := controllers.CreateBlog(ins)
	if err != nil {
		c.AbortWithError(http.StatusOK, err)
		return
	}
	c.JSON(http.StatusOK, ins)
}
