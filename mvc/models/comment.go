package models

type Comment struct {
	UserId  int    `json:"user_id"`
	BlogId  int    `json:"blog_id"`
	Content string `json:"content"`
}
