package conf_test

import (
	"gitee.com/hongxingli/vblog/conf"
	"github.com/BurntSushi/toml"
	"testing"
)

func TestTomDecodeFile(t *testing.T) {
	confObj := &conf.Config{}
	_, err := toml.DecodeFile("test/config.toml", confObj)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(confObj)
}

//测试load默认配置加载方式，假如config.toml不存在配置，走默认配置
func TestTomDecodeLoadFile(t *testing.T) {
	_, err := conf.LoadConfigFromToml("test/config.toml")
	if err != nil {
		t.Fatal(err)
	}
	//t.Log(config)

	//其他业务模块直接通过全局变量进行访问
	c := conf.C()
	t.Log(c)
}
