package conf

import (
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

var (
	config *Config //全局配置
)

//保护config不被随意乱改
func C() *Config {
	if config == nil {
		panic("先加载程序配置，LoadConfigFromToml/LoadConfigFromEnv")
	}
	return config
}

//默认配置 假如用户没有配置，让程序也能跑起来
//config 如何进行加载,选择加载方式

func LoadConfigFromToml(path string) (*Config, error) {
	conf := DefaultConfig()
	_, err := toml.DecodeFile(path, conf)
	if err != nil {
		return nil, err
	}
	config = conf
	return conf, nil
}
func LoadConfigFromEnv() (*Config, error) {
	conf := DefaultConfig()
	err := env.Parse(conf)
	if err != nil {
		return nil, err
	}
	config = conf
	return conf, nil
}
