package conf

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	orm_mysql "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"sync"
	"time"
)

type Config struct {
	//etc/config.toml[mysql] --> toml:mysql
	MySQL *MySQL `json:"mysql" toml:"mysql"`
	//[http] 默认配置
	Http *Http `json:"http" toml:"http"`
}

//默认配置 假如用户没有配置，让程序也能跑起来
func DefaultConfig() *Config {
	return &Config{
		MySQL: NewDefaultMysql(),
		//使用http默认配置，如果用户有写自然就会覆盖默认值
		Http: NewDefaultHttp(),
	}
}

//自定义打印
func (c *Config) String() string {
	//d, _ := json.Marshal
	d, _ := json.MarshalIndent(c, "", " ")
	return string(d)
}

type MySQL struct {
	Host     string `json:"host" toml:"host" env:"MYSQL_HOST"`
	Port     int    `json:"port" toml:"port" env:"MYSQL_PORT"`
	DB       string `json:"db" toml:"db" env:"MYSQL_DB"`
	Username string `json:"username" toml:"username" env:"MYSQL_USERNAME"`
	Password string `json:"password" toml:"password" env:"MYSQL_PASSORD"`

	MaxOpenConn int `toml:"max_open_conn" env:"MYSQL_MAX_OPEN_CONN"`
	MaxIdleConn int `toml:"max_idle_conn" env:"MYSQL_MAX_IDLE_CONN"`
	MaxLifeTime int `toml:"max_life_time" env:"MYSQL_MAX_LIFE_TIME"`
	MaxIdleTime int `toml:"max_idle_time" env:"MYSQL_MAX_IDLE_TIME"`

	lock sync.Mutex
	db   *gorm.DB
}

func (m *MySQL) ORM() *gorm.DB {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.db == nil {
		p, err := m.GetConnPool()
		if err != nil {
			panic(err)
		}

		m.db, err = gorm.Open(orm_mysql.New(orm_mysql.Config{
			Conn: p,
		}), &gorm.Config{
			PrepareStmt:            true,
			SkipDefaultTransaction: true,
		})
		if err != nil {
			panic(err)
		}
	}
	return m.db
}

//新增默认配置
func NewDefaultMysql() *MySQL {
	return &MySQL{
		Host:     "192.168.56.102",
		Port:     3306,
		DB:       "vblog",
		Username: "admin",
		Password: "123123",
	}
}

func (m *MySQL) GetConnPool() (*sql.DB, error) {
	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&multiStatements=true",
		m.Username, m.Password, m.Host, m.Port, m.DB)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, fmt.Errorf("connect to mysql<%s> error,%s", dsn, err.Error())
	}
	db.SetMaxOpenConns(m.MaxOpenConn)
	db.SetMaxOpenConns(m.MaxIdleConn)
	if m.MaxLifeTime != 0 {
		db.SetConnMaxLifetime(time.Second * time.Duration(m.MaxLifeTime))
	}
	if m.MaxIdleConn != 0 {
		db.SetConnMaxLifetime(time.Second * time.Duration(m.MaxIdleTime))
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err = db.PingContext(ctx); err != nil {
		return nil, fmt.Errorf("ping mysql[%s] error,%s", dsn, err.Error())
	}
	return db, nil
}

func NewDefaultHttp() *Http {
	return &Http{
		Host: "127.0.0.1",
		Port: 8080,
	}
}

type Http struct {
	Host string `json:"host" toml:"host" env:"HTTP_HOST"`
	Port int    `json:"port" toml:"port" env:"HTTP_PORT"`
}

func (h *Http) Address() string {
	return fmt.Sprintf("%s:%d", h.Host, h.Port)
}
