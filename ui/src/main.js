import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)
//加载ui组件
import ArcoVue from '@arco-design/web-vue'
//引入ui样式库
import '@arco-design/web-vue/dist/arco.css'
//配置vue实例 使用arcovue ui组件库
app.use(ArcoVue)

import ArcoVueIcon from '@arco-design/web-vue/es/icon'
app.use(ArcoVueIcon)

app.mount('#app')
