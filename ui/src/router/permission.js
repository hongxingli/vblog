import { state } from "@/stores/localstorage";
import {querySelector} from "@arco-design/web-vue/es/_utils/dom";
//业务守卫的业务逻辑
//只需要对backend进行登录鉴权
//net： 理解为router.push函数
export  var beforeEachHandler = function (to,from,next){
    // console.log(to,from)
    //使用inexof来判断当前url 是否以/backent开头
    if (to.path.indexOf('/backend') === 0 ){
        //需要判断当前用户是否已经登录
        if (!state.value.isLogin) {
            //如果没有登录 需要重定向到登录页面去
            //记录用户需要去往的目标页面 login?to=TagList
            next({name: "LoginPage",query: {to: to.name}})
            return
        }
    }
    next()
}