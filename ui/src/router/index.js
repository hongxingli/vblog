import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import {beforeEachHandler} from "./permission";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'LoginPage',
      component: () => import('@/views/login/LoginPage.vue')
    },
    {
      path: '/frontend',
      name: 'FrontendLayout',
      component: () => import('@/views/frontend/FrontendLayout.vue'),
      children: [
        {
          path: '/frontend/blog/list',
          name: 'BlogList',
          component: () => import('@/views/frontend/BlogList.vue')
        }
      ]
    },
    // {
    //   path: "/frontend/test",
    //   name: "test",
    //   component: () => import("@/views/frontend/test.vue"),
    //   children: [
    //     {
    //       path: "/frontend/test/v2",
    //       name: "test",
    //       component: () => import("@/views/frontend/test2.vue"),
    //     }
    //   ]
    // },
    {
      path: '/backend',
      name: 'BackendLayout',
      component: () => import('@/views/backend/BackendLayout.vue'),
      children: [
        {
          // path: "navigation" 等于 path: "/backend/navigation"
          path: 'admin',
          name: 'admin',
          component: () => import('@/views/backend/AdminView.vue')
        },
        {
          path: 'blog',
          name: 'BlogList',
          component: () => import('@/views/backend/blog/BlogList.vue')
        },
        {
          path: 'tag',
          name: 'TagList',
          component: () => import('@/views/backend/tag/TagList.vue')
        },
        {
          path: 'comment',
          name: 'CommentList',
          component: () => import('@/views/backend/comment/CommentList.vue')
        }
      ]
    },
    {
      path: '/errors/403',
      name: '403',
      component: () => import('@/views/errors/403.vue')
    },
    {
      path: '/:pathMatch(.*)*',
      name: '404',
      component: () => import('@/views/errors/404.vue')
    }
  ]
})

//对router进行导航守卫的设置
//全局前置守卫
router.beforeEach(beforeEachHandler)

export default router
