import { useStorage } from '@vueuse/core'

//把localstorage封装成一个响应式的ref对象
export var state = useStorage('my-store', { isLogin: false }, localStorage, { mergeDefaults: true })
