# vblog前端展示  
采用vue + arco design
## 项目初始化
```sh
PS E:\disk\gomageedu\golang11\week17\vblog> npm init vue@latest

Vue.js - The Progressive JavaScript Framework

√ Project name: ... ui
√ Add TypeScript? ... No
√ Add JSX Support? ... No
√ Add Vue Router for Single Page Application development? ... Yes
√ Add Pinia for state management? ... Yes
√ Add Vitest for Unit Testing? ... No 
√ Add an End-to-End Testing Solution? » No
√ Add ESLint for code quality? ... Yes
√ Add Prettier for code formatting? ...Yes

Scaffolding project in E:\disk\gomageedu\golang11\week17\vblog\ui...

Done. Now run:

  cd ui
  npm install
  npm run format
  npm run dev
  
```

## 配置国内源
```sh
npm install yrm
yrm ls  //windows不能用以后查吧
yrm use taobao  //切换淘宝源
```
## 清理样例页面  
components 全部清理掉  
app.vue 只留下面的内容  
```vue
<script setup>
</script>

<template>
  <div>app route</div>
</template>

<style scoped>

</style>
```
清理about视图  
homeview.vue 视图修改成如下  
```vue
<script setup>
</script>

<template>
<div>home</div>
</template>

```
router/index.js 路由清理掉about路由  
重新设置样式 assets/base.css main.css 删除logo  
base.css设置全局样式
```css
* {
  box-sizing: border-box;
}

html,
body {
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
  font-size: 14px;
  background-color: var(--color-bg-1);
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
}

#app {
  width: 100%;
  height: 100%;
}
```
main.css 只保留基础样式
```css
@import "./base.css";
```
## 安装ui插件Arco Design
```shell
# npm
npm install --save-dev @arco-design/web-vue
# yarn
yarn add --dev @arco-design/web-vue
```
在main.js 加载ui组件
```vue
//加载ui组件
import ArcoVue from '@arco-design/web-vue';
//引入ui样式库
import '@arco-design/web-vue/dist/arco.css';
//配置vue实例 使用arcovue ui组件库
app.use(ArcoVue)
```
测试样式是否生效 app.vue
```vue
    <a-space>
      <a-button type="primary">Primary</a-button>
      <a-button>Secondary</a-button>
      <a-button type="dashed">Dashed</a-button>
      <a-button type="outline">Outline</a-button>
      <a-button type="text">Text</a-button>
    </a-space>
```
引入图标库 icon,验证同上面一样
```vue
import ArcoVueIcon from '@arco-design/web-vue/es/icon';
app.use(ArcoVueIcon)
```
views 新增errors/404.view和403.view页面
```vue
404页面

<template>
  <a-result status="404" subtitle="Whoops, that page is gone.">
    <template #extra>
      <a-space>
        <a-button type="primary">Back</a-button>
      </a-space>
    </template>
  </a-result>
</template>

403页面
<template>
  <a-result
      status="403"
      subtitle="Access to this resource on the server is denied."
  >
    <template #extra>
      <a-space>
        <a-button type="primary">Back</a-button>
      </a-space>
    </template>
  </a-result>
</template>
```
app.vue新增路由出口,添加完路由出口就可以验证访问404 403的效果了
```vue
<!--  路由出口，路由匹配到的组件将渲染在这里，当前路由指向那个页面，router view就显示那个页面的组件-->
<!--  router view 像一个指针，指向的是当前访问的路由页面-->
  <router-view></router-view>>
```
路由跳转  
使用routerlink组件来进行路由跳转
```vue
  <router-link to="/"> Go To Home </router-link>
  <router-link to="/frontend/test"> Go To Test </router-link>
```
使用编程的方式进行路由跳转
```vue
<script setup>
import {useRouter} from "vue-router";

//useRouter返回当前router实例对象
const router = useRouter()
console.log(router)

</script>

<!--  使用编程方式 操作js来完成路由导航 router.to("/")-->
    <a-button @click="router.push('/')">Go To Home</a-button>
    <a-button @click="router.push('/frontend/test')">Go To Test</a-button>
```
路由嵌套 添加子路由
```vue
index.js

    {
path: "/frontend/test",
name: "test",
component: () => import("@/views/frontend/test.vue"),
children: [
{
path: "/frontend/test/v2",
name: "test",
component: () => import("@/views/frontend/test2.vue"),
}
]
},

test.vue
<template>
  <div>test</div>
  <br>
  <router-view></router-view>
</template>

test2.vue
<template>
  <div>test2</div>
</template>

<script>
export default {
  name: "test"
}
</script>

<style scoped>

</style>
```

## 登录状态处理
利用localstorage获取一个响应式对象（ref）
```js
import {useStorage} from "@vueuse/core"

//把localstorage封装成一个响应式的ref对象
export var state = useStorage(
    'my-store',
    {isLogin: false},
    localStorage,
    {mergeDefaults: true}
)
```
login页面 增加状态保存
```js
import{state} from '@/stores/localstorage'

    state.value.isLogin=true
```
顶部导航 app.vue使用
```js
import {state} from "@/stores/localstorage"

const Logout = () =>{
    state.value.isLogin=false
    router.push({name:"LoginPage"})
}

<a-button v-if = "state.isLogin">后台管理</a-button>
<a-button v-if = "state.isLogin" @click="Logout">注销</a-button>
<a-button v-if = "!state.isLogin" @click="Login">登录</a-button>

```

## 导航守卫
可以理解为python的装饰器
通俗来讲：路由守卫就是路由跳转过程中的一些生命周期函数（钩子函数），我们可以利用这些钩子函数帮我们实现一些需求，比如跳转前是否验证登录等，这就是导航守卫。
用户访问到页面执勤（vue Router），需要做权限判断
### 跳转参数
1、跳转需要带上参数
```js
        //需要判断当前用户是否已经登录
        if (!state.value.isLogin) {
            //如果没有登录 需要重定向到登录页面去
            //记录用户需要去往的目标页面 login?to=TagList
            next({name: "LoginPage",query: {to: to.name}})
            return
        }
```
2、LoginPage需要处理query的参数
```js
    const route = useRoute()

//默认跳bloglist
    let to = 'BlogList'
    //获取当亲url的query参数，可以通过获取当前路由 /login?to=TagList
    //useRoute vue-router 来提供获取当前页面的路由对象，Route对象
    // console.log(route.query)  //{to: 'TagList'}
    if (route.query.to) {
      to = route.query.to
    }
    router.push({ name:to })
```