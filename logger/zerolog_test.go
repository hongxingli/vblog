package logger_test

import (
	"gitee.com/hongxingli/vblog/logger"
	"testing"
)

func TestZerologgerDebug(t *testing.T) {
	logger.L().Debug().Str("host", "127.0.0.1").Msgf("hello zerolog test")
}
